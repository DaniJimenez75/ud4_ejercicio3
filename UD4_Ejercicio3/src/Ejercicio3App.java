/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int X = 5;
		int Y = 7;
		double N = 8.5;
		double M = 10.2;
		
		//Valor de cada variable
		System.out.println("El valor de X es: "+X);
		System.out.println("El valor de Y es: "+Y);
		System.out.println("El valor de N es: "+N);
		System.out.println("El valor de M es: "+M);
		
		//Suma de X+Y
		int suma = X+Y;
		System.out.println("La suma de X+Y es: ");
		
		//La diferencia de X-Y
		int resta = X-Y;
		System.out.println("La resta de X-Y es:"+resta);
		
		//El producto X*Y
		int producto = X*Y;
		System.out.println("El producto de X*Y es: "+producto);
		
		//El cociente
		int cociente = 	X/Y;
		System.out.println("El cociente de X/Y es: "+cociente);
		
		//El resto
		int resto = X%Y;
		System.out.println("El resto de X/Y es:"+resto);
		
		//La suma de N+M
		double suma2 = N+M;
		System.out.println("La suma de N+M es:"+suma2);
		
		//La diferencia de N-M
		double resta2 = N-M;
		System.out.println("La resta de N-M es:"+resta2);

		//El producto de N*M
		double producto2 = N*M;
		System.out.println("El producto de N*M es:"+producto2);
		
		//El cociente de N/M
		double cociente2 = N/M;
		System.out.println("El cociente de N/M es:"+cociente2);
		
		//El resto de N%M
		double resto2 = N%M;
		System.out.println("El resto de N%M es:"+resto2);
		
		//La suma de X+N
		double suma3 = X+N;
		System.out.println("La suma de X+N es: "+suma3);
		
		//El cociente Y/M
		double cociente3 = Y/M;
		System.out.println("El cociente de Y/M es: "+cociente3);
		
		//El resto Y%M
		double resto3 = Y%M;
		System.out.println("El resto de Y%M es: "+resto3);
		
		//El doble de cada variable
		int dobleDeX = X*2;
		int dobleDeY = Y*2;
		double dobleDeN = N*2;
		double dobleDeM = M*2;
		
		System.out.println("El doble de X es: "+dobleDeX);
		System.out.println("El doble de Y es: "+dobleDeY);
		System.out.println("El doble de N es: "+dobleDeN);
		System.out.println("El doble de M es: "+dobleDeM);


		//La suma de todas las variables
		double SumaTotal = X+Y+N+M;
		System.out.println("La suma de todas las variables es: "+SumaTotal);
		
		//El producto de todas las variables
		
		double productoTotal = X*Y*N*M;
		System.out.println("El producto de todas las variables es: "+productoTotal);

	}

}
